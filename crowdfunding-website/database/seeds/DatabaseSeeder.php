<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;

use App\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableSeeder::class);

        //DB_ROLES
        Role::create([
            'name' => 'admin',
        ]);
        Role::create([
            'name' => 'user',
        ]);

        // //DB_USERS
        // DB::table('users')->insert([
        //     'id' => '00a9d3f9-500e-4efa-aa94-7ab67575f515',
        //     'name' => Str::random(10),
        //     'username' => 'coba11',
        //     'email' => Str::random(10).'@gmail.com',
        //     'email_verified_at' => 0,
        //     'role_id' => '7908b61e-24b2-409c-aea7-5f6a85a7def9',
        //     'created_at' => Carbon::now(),
        //     'updated_at' => Carbon::now(),
        // ]);
        // DB::table('users')->insert([
        //     'id' => '74288856-b405-443b-a65b-7b651caee6ed',
        //     'name' => Str::random(10),
        //     'username' => 'coba22',
        //     'email' => Str::random(10).'@gmail.com',
        //     'email_verified_at' => 1,
        //     'role_id' => '7908b61e-24b2-409c-aea7-5f6a85a7def9',
        //     'created_at' => Carbon::now(),
        //     'updated_at' => Carbon::now(),
        // ]);
        // DB::table('users')->insert([
        //     'id' => 'd36b855b-d54d-4439-866f-fbe005e670bb',
        //     'name' => Str::random(10),
        //     'username' => 'coba33',
        //     'email' => Str::random(10).'@gmail.com',
        //     'email_verified_at' => 1,
        //     'role_id' => 'a28ddc79-decd-4790-be72-fb5be819b714',
        //     'created_at' => Carbon::now(),
        //     'updated_at' => Carbon::now(),
        // ]);

        // //DB_OTP_CODES
        // DB::table('otp_codes')->insert([
        //     'id' => Str::uuid(),
        //     'user_id' => '00a9d3f9-500e-4efa-aa94-7ab67575f515',
        // ]);
        // DB::table('otp_codes')->insert([
        //     'id' => Str::uuid(),
        //     'user_id' => 'd36b855b-d54d-4439-866f-fbe005e670bb',
        // ]);

    }
}
