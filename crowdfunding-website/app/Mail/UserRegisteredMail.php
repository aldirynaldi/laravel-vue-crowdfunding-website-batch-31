<?php

namespace App\Mail;

use App\User;
use App\Otp_code;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegisteredMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $userId = $this->user->id;
        $findOtpUser = Otp_code::where('user_id', $userId)->first()->otp;

        return $this->from('example@example.com')
                    ->view('send_mail_otp')
                    ->with([
                        'name' => $this->user->name,
                        'otp' => $findOtpUser,
                    ]);
    }
}
