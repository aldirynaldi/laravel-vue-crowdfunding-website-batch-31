<?php

namespace App;

use App\Role;
use App\Otp_code;
use App\Traits\UsesUuid;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

use Carbon\Carbon;

use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable, UsesUuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username', 
        'email', 
        'password', 
        'role_id',
        'photo_profile',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // protected $primaryKey = 'id';
    // protected $keyType = 'string';
    // public $incrementing = false;

    // protected static function boot(){
    //     parent::boot();

    //     static::creating( function($model) {
    //         if ( empty($model->{$model->getKeyName()})) {
    //             # code...
    //             $model->{$model->getKeyName()} = Str::uuid();
    //         }
    //     });
    // }

    

    // one user memiliki one rule
    public function role(){
        return $this->belongsTo(Role::class);
    }

    // one user hanya memiliki one otp code
    public function otp_code(){
        return $this->belongsTo(Otp_code::class);
    }

    

    public function get_role_admin() {
        
        $role_id =  Role::where('name', 'admin')->first()->id;

        return $role_id;
    }

    public function get_role_user() {
        
        $role_id =  Role::where('name', 'user')->first()->id;

        return $role_id;
    }

    public function isAdmin() {
        if ($this->role_id == $this->get_role_admin()) {
            # code...
            return true;
        }

        return false;
    }

    public function generateOtpCode(){

        do {
            # code...
            $otp_random = mt_rand(100000, 999999);
            $check = Otp_code::where('otp', $otp_random)->first(); 
        } while ($check);

        $now = Carbon::now();

        // create ot update tabel DB otp_code
        $createTbOtp = Otp_code::updateOrCreate(
            
            //harus urutan 1
            ['user_id' => $this->id],   

            //baru urutan selanjutnya
            [
                'otp' => $otp_random,
                'valid_until' => $now->addMinutes(5),
            ],
        );

    }

    // fungsi ini digunakan ketika 1 record dibuat secara default role_id akan bernilai "user"
    protected static function boot(){
        parent::boot();
    
        static::creating( function($model) {
            $model->role_id = $model->get_role_user();
        });

        static::created(function($model){
            $model->generateOtpCode();
        });
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
