<?php

namespace App;

use App\users;
use App\Traits\UsesUuid;
use Illuminate\Support\Str;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use UsesUuid;
    //
    protected $fillable = [
        'name'
    ];
    
    protected $primaryKey = 'id';
    // protected $keyType = 'string';
    // public $incrementing = false;

    // protected static function boot(){
    //     parent::boot();

    //     static::creating( function($model) {
    //         if ( empty($model->{$model->getKeyName()})) {
    //             # code...
    //             $model->{$model->getKeyName()} = Str::uuid();
    //         }
    //     });
    // }

    // one role memiliki many users
    public function users() {
        return $this->hasMany(User::class);
    }
}
