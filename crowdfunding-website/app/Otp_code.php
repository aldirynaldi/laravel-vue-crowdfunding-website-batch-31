<?php

namespace App;

use App\Traits\UsesUuid;

use Illuminate\Database\Eloquent\Model;

class Otp_code extends Model
{
    use  UsesUuid;
    //
    protected $fillable = [
        'name',
        'otp',
        'user_id',
        'valid_until',
    ];

    protected $primaryKey = 'id';
    // protected $keyType = 'string';
    // public $incrementing = false;

    // protected static function boot(){
    //     parent::boot();

    //     static::creating( function($model) {
    //         if ( empty($model->{$model->getKeyName()})) {
    //             # code...
    //             $model->{$model->getKeyName()} = Str::uuid();
    //         }
    //     });
    // }

    // one otp code harus memiliki one user
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function generateOtp(){
        return rand(100000, 999999);
    }
}
