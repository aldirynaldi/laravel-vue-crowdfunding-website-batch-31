<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProfileController extends Controller
{
    //
    public function show() {

        $data['user'] = auth()->user();

        return response()->json([
            'respone_code' => '00',
            'response_message' => 'profile berhasil ditampilkan',
            'data' => $data,
        ], 200);
    }

    public function update(Request $request){
        $user = auth()->user();

        if ($request->hasFile('photo_profile')) {
            # code...
            $photo_profile = $request->file('photo_profile');
            $photo_profile_extension = $photo_profile->getClientOriginalExtension();
            $photo_profile_name = Str::slug($user->name,'-') . '-' . $user->id . '.' . $photo_profile_extension;
            $photo_profile_folder = '/photos/users/photo-profile/';
            $photo_profile_location = $photo_profile_folder . $photo_profile_name;

            try {
                //code...
                $photo_profile->move(public_path($photo_profile_folder), $photo_profile_name);

                $user->update([
                    'photo_profile' => $photo_profile_location,
                ]);

            } catch (\Throwable $th) {
                //throw $th;
                return response()->json([
                    'respone_code' => '01',
                    'response_message' => 'profile gagal di update',
                ], 200);
            }

            
        }

        $user->update([
            'name' => $request->name,
        ]);

        $data['user'] = $user;

        return response()->json([
            'respone_code' => '00',
            'response_message' => 'profile berhasil di update',
            'data' => $data,
        ], 200);

    }
}
