<?php

namespace App\Http\Controllers\Auth;

use App\Otp_code;
use App\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\VerificationRequest;

use Carbon\Carbon;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(VerificationRequest $request)
    {
        $otp_code = Otp_code::where('otp', $request->otp)->first();

        if (!$otp_code) {
            # code...
            return response()->json([
                'respone_code' => '01',
                'response_message' => 'code otp tidak ditemukan',
            ], 200);

        }
        
        $now = Carbon::now();

        if ($now > $otp_code->valid_until) {
            # code...
            return response()->json([
                'respone_code' => '01',
                'response_message' => 'code otp sudah tidak berlaku',
            ], 200);
        }

        //update user
        $user = User::find($otp_code->user_id);
        $user->email_verified_at = Carbon::now();
        $user->save();
        //delete otp yang sudah tidak digunakan
        $otp_code->delete();

        $data['user'] = $user;

        return response()->json([
            'respone_code' => '00',
            'response_message' => 'user berhasil diverifikasi',
            'user' => $data,
        ], 200);
    }
}
