<?php

namespace App\Http\Controllers\Auth;

use App\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;

use App\Events\UserRegisteredEvent;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request) //-> request\RegisterRequest.php
    {
        //
        // request()->validate([
            
        // ]);

        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
        ]);

        $data['user'] = $user;

        
        event(new UserRegisteredEvent($user));

        return response()->json([
            'respone_code' => '00',
            'response_message' => 'user baru berhasil didaftarkan, silahkan cek email untuk melihat kode otp',
            'data' => $data,
        ], 200);
    }
}
