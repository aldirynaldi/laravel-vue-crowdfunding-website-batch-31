<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegenerateOtpCodeRequest;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegenerateOtpCodeRequest $request)
    {
        //
        $user = User::where('email', $request->email)->first();
        $user->generateOtpCode();

        $data['user'] = $user;

        return response()->json([
            'respone_code' => '00',
            'response_message' => 'otp berhasil di regenerate, silahkan cek email untuk melihat code otp',
            'data' => $data,
        ], 200);
    }
}
