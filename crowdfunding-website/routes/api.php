<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([
    'namespace' => 'Auth',
    'middleware' => 'api',
], function () {
    Route::post('auth/register', 'RegisterController');
    Route::post('auth/regenerate-otp', 'RegenerateOtpCodeController');
    Route::post('auth/verification', 'VerificationController');
    Route::post('auth/update-password', 'UpdatePasswordController');
    Route::post('auth/login', 'LoginController');
});

Route::group([
    'namespace' => 'Profile',
    'middleware' => ['api', 'email_verified', 'auth:api'],
], function () {
    Route::get('profile/show', 'ProfileController@show');
    Route::post('profile/update', 'ProfileController@update');
});
