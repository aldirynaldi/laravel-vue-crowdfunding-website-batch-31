<?php

abstract class Fight {
    public $attackPower;
    public $defencePower;

    public function __construct($attackPower = 0, $defencePower = 0) {
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }

    abstract public function serang($musuh, $isAttack);

    abstract public function diserang();
}


abstract class Hewan extends Fight{
    public $darah = 50;
    public $name;
    public $jml_kaki;
    public $keahlian;
    public $terserang = false;

    public function __construct($nama = "hewan", $jml_kaki = 0, $keahlian = "makan", $attackpower = 0, $defencePower = 0){
        $this->name = $nama;
        $this->jml_kaki = $jml_kaki;
        $this->keahlian = $keahlian;

        $this->attackPower = $attackpower;
        $this->defencePower = $defencePower;
    }

    abstract public function atraksi();

    public function serang($musuh, $isAttack){
        $terserang = $isAttack;
        return "{$this->name} sedang menyerang $musuh";
    }
    public function diserang(){
        $darah = $darah; 
        return "{$this->name} sedang diserang ";
    }

    
}


class Elang extends Hewan {

    public function atraksi(){
        return "{$this->name} sedang {$this->keahlian}";
    }

}

class Harimau extends Hewan {

    public function atraksi(){
        return "{$this->name} sedang {$this->keahlian}";
    }
}

$elang3 = new Elang("elang_3", 2, 'terbang tinggi', 10, 5);
$harimau1  = new Harimau("harimau_1", 4, 'lari cepat', 7, 8);

echo $elang3->atraksi();
echo $harimau1->atraksi();
echo $elang3->serang($harimau1->name);


?>